const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let hasilHitung ;
let jumlahAngka ;
const ket = (`
Selamat datang di aplikaasi calculator. 
pilih proses yang ingin dilakukan :
    1. Penjumlahan
    2. Pengurangan
    3. Perkalian
    4. Pembagian
    5. akar
    6. kuadrat
    7. pangkat
    8. luas persegi
    9. volume kubus
    0. volume tabung

# untuk menyelesaikan program jangan pilih angka 0 sampai 9
            
kamu pilih yang mana : `)

function question(question) {
    return new Promise((resolve) => {
      rl.question(question, (data) => {
        resolve(data);
      });
    });
}

let luasPersegi = (s) => {
    let luas = Number(s) * Number(s);
    return luas;
};

let volKubus = (s) => {
    let vol = Number(s) ** 3;
    return vol;
}

let volTabung = (r, t) => {
    let phi = 3.14;
    let vol = phi * Number(r)**2 * Number(t);
    return vol;
}

let tambah = (a, b) => {
    return Number(a) + Number(b);
}

let kurang = (a, b) => {
    return Number(a) - Number(b);
}

let kali = (a, b) => {
    return Number(a) * Number(b);
}

let bagi = (a, b) => {
    return Number(a) / Number(b);
}

let akar = (a) => {
    return Math.sqrt(Number(a));
}

let kuadrat = (a) => {
    return Number(a) ** Number(a);
}

let pangkat = (a, b) => {
    return Number(a) ** Number(b);
}

async function main(){
    try {
        let menu = await question(ket)
        console.log(`kamu pilih = ${menu}`);
        switch (menu){
            case '1':
                let a = await question(`input bil1 : `);
                let b = await question(`input bil2 : `);
                console.log(`hasil ${a} + ${b} = ` + tambah(a,b));
                main();
                break;
            case '2':
                let c = await question(`input bil1 : `);
                let d = await question(`input bil2 : `);
                console.log(`hasil ${c} - ${d} = ` + kurang(c,d));
                main();
                break;
            case '3':
                let e = await question(`input bil1 : `);
                let f = await question(`input bil2 : `);
                console.log(`hasil ${e} * ${f} = ` + kali(e,f));
                main();
                break;
            case '4':
                let g = await question(`input bil1 : `);
                let h = await question(`input bil2 : `);
                console.log(`hasil ${g} / ${h} = ` + bagi(g,h));
                main();
                break;
            case '5':
                let i = await question(`input bil1 : `);
                console.log(`hasil √${i} = ` + akar(i));
                main();
                break;
            case '6':
                let j = await question(`input bil1 : `);
                console.log(`hasil ${j} ** ${j} = ` + kuadrat(j));
                main();
                break;
            case '7':
                let k = await question(`input bil1 : `);
                let l = await question(`input bil2 : `);
                console.log(`hasil ${k} ** ${l} = ` + pangkat(k,l));
                main();
                break;
            case '8':
                let sisi = await question(`input sisi : `);
                console.log(`hasil luas persegi = ` + luasPersegi(sisi));
                main();
                break; 
            case '9':
                let s = await question(`input sisi : `);
                console.log(`hasil vol kubus = ` + volKubus(s));
                main();
                break;
            case '0':
                let r = await question(`input r : `);
                let t = await question(`input t : `);
                console.log(`hasil vol tabung = ` + volTabung(r,t));
                main();
                break;                               
            default:
                console.log('program close');
                rl.close();   
        }
    } catch(err) {
        console.log(err);
        rl.close;
    }
}

main();

